'use strict';

module.exports = {

    /**
     * List Template
     *
     * https://developers.facebook.com/docs/messenger-platform/send-messages/template/list
     *
     * @param {object} element
     * @param {string} topElementStyle
     * @returns {*|Message|{}}
     */
    sourceEvent: (element, topElementStyle) => {
        if (! topElementStyle || topElementStyle.toLowerCase() !== 'large') {
            topElementStyle = "compact";
        }

        let data = {
            facebook: {
                attachment: {
                    type: "template",
                    payload: {
                        template_type: "list",
                        top_element_style: topElementStyle,
                        elements: [
                            {
                                title: "Classic T-Shirt Collection",
                                subtitle: "See all our colors",
                                image_url: "https://icdn6.digitaltrends.com/image/google-deepmind-artificial-intelligence-2-720x720.jpg",
                                buttons: [
                                    {
                                        title: "More Info",
                                        type: "postback",
                                        payload: "payload"
                                    }
                                ]
                            },
                            {
                                title: "Classic T-Shirt Collection",
                                subtitle: "See all our colors",
                                image_url: "https://icdn6.digitaltrends.com/image/google-deepmind-artificial-intelligence-2-720x720.jpg",
                                buttons: [
                                    {
                                        title: "More Info",
                                        type: "postback",
                                        payload: "payload"
                                    }
                                ]
                            },
                            {
                                title: "Classic T-Shirt Collection",
                                subtitle: "See all our colors",
                                image_url: "https://icdn6.digitaltrends.com/image/google-deepmind-artificial-intelligence-2-720x720.jpg",
                                buttons: [
                                    {
                                        title: "More Info",
                                        type: "postback",
                                        payload: "payload"
                                    }
                                ]
                            }
                        ],
                        buttons: [
                            {
                                title: "View More",
                                type: "postback",
                                payload: "payload"
                            }
                        ]
                    }
                }
            }
        };

        return data;
    }

}
