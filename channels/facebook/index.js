'use strict';

const rp = require('request-promise');

const channelConfig = require('../../config/channels');

module.exports = {

    /**
     * Persistent Menu
     *
     * The persistent menu allows you to have an always-on user interface element inside Messenger conversations.
     * This is an easy way to help people discover and access the core functionality of your Messenger
     * bot at any point in the conversation.
     *
     * To set the persistent menu, send a POST request to the Messenger Profile API to
     * set the persistent_menu property of your bot's Messenger profile.
     *
     * The property should include an array of objects with a set of
     * up to3 buttons to include in the menu in a call_to_actions array.
     *
     * For the persistent menu to appear, the following must be true =>
     * The person must be running Messenger v106 or above on iOS or Android.
     * The Facebook Page the Messenger bot is subscribe to must be published.
     * The Messenger bot must be set to "public" in the app settings.
     * The Messenger bot must have the pages_messaging permission.
     * The Messenger bot must have a get started button set.
     *
     * Supported Buttons:
     * The persistent menu is composed of an array of buttons. The following button types are supported in the
     * persistent menu
     * web_url - Specifies the item is a URL button.
     * postback - Specifies the item is a postback button.
     * nested - Specifies the item opens a nested menu. Supports up to 5 buttons
     *
     * https://developers.facebook.com/docs/messenger-platform/send-messages/persistent-menu
     *
     * @param {object} persistentMenu
     */
    setPersistentMenu: (persistentMenu) => {
        let payload = {
            "persistent_menu": [
                persistentMenu
            ]
        }

        callMessengerProfileAPI(payload);
    },

    /**
     * "Get Started" Button
     *
     * A bot's welcome screen can display a Get Started button.
     * When this button is tapped, the Messenger Platform will send a messaging_postbacks event to your webhook.
     * Bots that add the button may also wish to configure their greeting text.
     *
     * The welcome screen is only shown the first time the user interacts with the Page on Messenger.
     * While the Facebook app is in development mode, the welcome screen will only be visible to
     * people with the administrator, developer, and tester app roles.
     *
     * https://developers.facebook.com/docs/messenger-platform/reference/messenger-profile-api/get-started-button
     *
     * @param {string} buttonPostback
     *
     */
    setGetStartedButton: (buttonPostback) => {
        let payload = {
            "get_started": {
                "payload": buttonPostback
            }
        }

        callMessengerProfileAPI(payload);
    },

    /**
     * Welcome Screen Greeting Text
     *
     * https://developers.facebook.com/docs/messenger-platform/reference/messenger-profile-api/greeting
     *
     * @param {array} greetings
     */
    setGreetingText: (greetings) => {
        let payload = {
            "greeting": greetings
        }

        callMessengerProfileAPI(payload);
    },

}


/**
 *
 * @param {object} payload
 */
function callMessengerProfileAPI(payload) {
    rp({
        uri:    'https://graph.facebook.com/v2.6/me/messenger_profile',
        qs:     {
            access_token: channelConfig.facebook.pageAccessToken
        },
        method: 'POST',
        body:   payload,
        json:   true,
    }).then(function(res) {
        console.log(res.result);
    }).catch(function(error) {
        console.log(`ERROR: ${error}`)
    });
}
