'use strict';

require('crashreporter').configure({
    outDir: './storage/logs', // (default: cwd)
    exitOnCrash: true, // have crash reporter use exit(1)
    maxCrashFile: 10, // Clean up old files. (default: 5 files),
    hiddenAttributes: [
        'execPath', 'requireCache', 'activeHandle'
    ],
});

require('dotenv').config({silent: true});

/**
 * Core Modules
 */
//const restify = require('restify');
const builder = require('botbuilder');
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

/**
 * Internal Modules
 */
const config = require('./config');
const stateData = require('./database/statedata');
const facebook = require('./channels/facebook');

const mongo = require('./database/mongodb');

/**
 * Globals
 */
global.isProduction = config.app.env === 'local' ? false : true;
global.botlog = require('./logger');

/**
 * NLU Services
 */
//const LUIS = require('./services/NLP/luis');
//const QnAMaker = require('./services/NLP/qna-maker');
//const dialogflow = require('./services/NLP/dialogflow');


/**
 * MongoDB Connection
 */
mongo.connect();


// =========================================================
// Server Setup
// =========================================================

let server = express();
const port = config.app.port;
server.set('port', port);
server.set('view engine', 'ejs'); // Use the EJS view engine
server.set('views', path.resolve(process.cwd(), './public/views')); // Serve EJS view files in the public directory
server.use(express.static('public')); // Serve static files in the public directory
server.use(bodyParser.urlencoded({extended: false})); // Process application/x-www-form-urlencoded
server.use(bodyParser.json());
server.listen(config.app.port, function () {
    botlog.debug(`Server is up! Listening on: ${server.settings.port}`);
});

/**
 * Application Routes
 */
server.get('/', (_, res) => {
    res.render('./index', {
        pageTitle: 'Demo Bot'
    });
});

server.get('/terms', (_, res) => {
    res.render('./terms', {
        pageTitle: 'Terms of Service'
    });
});

server.get('/privacy', (_, res) => {
    res.render('./privacy', {
        pageTitle: 'Privacy Policy'
    });
});

server.get('/facebook/setmenu', (_, res) => {
    let menu = {
        "locale":"default",
        "composer_input_disabled": false,
        "call_to_actions":[
            {
                "title":"Go To Website",
                "type":"web_url",
                "url":"https://haalamedia.com",
                "webview_height_ratio":"full"
            },
        ]
    };
    facebook.setPersistentMenu(menu);
    res.status(200).end();
});


// =========================================================
// Bot Setup
// =========================================================

/**
 * Facebook Webhook Connector
 */
/*import { FacebookConnector } from "botbuilder-facebook-connector";
import * as Bot from "messenger-bot";
let messengerBot = new Bot({
    token: process.env.FacebookPageAccessToken,
    verify: process.env.FacebookVerifyToken,
});
let connector = new FacebookConnector(messengerBot);*/


/**
 * Microsoft Bot Framework authentication
 */
let microsoftAuth;
if (isProduction) {
    const appId = config.azure.microsoft.appId;
    const appPassword = config.azure.microsoft.appPassword;
    if (! (appId || appPassword)) {
        console.error('Missing Microsoft App credentials.');
    }
    microsoftAuth = {
        appId:       appId,
        appPassword: appPassword
    }
}


/**
 * Bot Connector
 */
let connector = new builder.ChatConnector(microsoftAuth);

let bot = new builder.UniversalBot(connector)
let stateDataStorge = stateData();
if (! stateDataStorge) {
    botlog.debug('Error building StateData storage. Falling back to In-Memory State Data.');

    stateDataStorge = new builder.MemoryBotStorage();
}
bot.set('storage', stateDataStorge);
server.post('/api/messages', connector.listen());


/**
 * Bot Middleware
 */
// Anytime the major version is incremented any existing conversations will be restarted.
bot.set(builder.Middleware.dialogVersion({ version: 1.0, resetCommand: /^reset/i }));


/**
 * Dialog Library
 */
bot.library(require('./dialogs/main').createLibrary());
bot.library(require('./dialogs/videos').createLibrary());
bot.library(require('./dialogs/audio').createLibrary());
bot.library(require('./dialogs/events').createLibrary());
bot.library(require('./dialogs/faq').createLibrary());
bot.library(require('./dialogs/shop').createLibrary());


/**
 * Dialog Recognizers
 */
//const dialogflowRecognizer = new dialogflow(config.nlp.dialogflow.token);

let intentDialog = new builder.IntentDialog({
    recognizers: [
        //luisRecognizer,
        //dialogflowRecognizer
    ]
});

intentDialog.matches(/^(test)$/i, 'main:/test')
            .matches(/^(menu)$/i, 'main:/features')
            .matches(/^(music)$/i, 'main:/music')
            .onDefault('main:/fallback');

bot.dialog('/', intentDialog);

/**
 * First Run Dialog
 * This will, by default, always be the first dialog to run whenever a message is received,
 * unless setting a parameter check to bypass it.
 */
bot.dialog('firstRun', [
    function(session, args, next) {
        botlog.debug('Booting...');

        // Set First Run variable to stop future messages from hitting this dialog.
        session.userData.firstRun = true;

        // Initialize the collection of pages seen by the user
        session.userData.seenDialogs = [];

        // Perform any channel-specific activities
        //

        botlog.debug('First Run complete.');
        session.endDialog();

        // Begin the normal root dialog handling
        session.beginDialog('/');
    }
]).triggerAction({
    onFindAction: function(context, callback) {
        // Only trigger firstRun if we've never seen the user before
        if (! context.userData.firstRun) {
            // Return a score of 1.1 to ensure the first run dialog runs
            callback(null, 1.1);
        } else {
            callback(null, 0.0);
        }
    }
});


/**
 * Log bot errors
 */
bot.on('error', function(error) {
    botlog.error(error);
});
