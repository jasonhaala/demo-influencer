'use strict'

const apiai = require('apiai');

const nlpConfig = require('../../../../config/nlp');

let dialogflow;
if (! nlpConfig.dialogflow.token) {
    botlog.error('Missing required config value: Dialogflow client access token.');
} else {
    dialogflow = apiai(nlpConfig.dialogflow.token);
}

module.exports = {

    textRequest: function(session, languageCode, callback) {
        let msgText = session.message.text;
        let userId = session.userData.id;
        let request = dialogflow.textRequest(msgText, {
            sessionId: userId
        });

        processRequest(request, function(error, result) {
            if (error) {
                callback(true, null);
            } else {
                callback(null, result);
            }
        });
    }

}


/**
 * Send an incoming message text to Dialogflow to process a response
 *
 * @param chatPlatform
 * @param senderId
 * @param messageText
 */
function processRequest(request, callback) {
    console.log('Processing Dialogflow request...');

    // Get the response from Dialogflow
    request.on('response', function(response) {
        let result = response.result;
        let score = result.score;
        //let speechText = result.fulfillment.speech;
        //let responseData = result.fulfillment.data;
        let messages = result.fulfillment.messages;
        let action = result.action;
        let contexts = result.contexts;
        let parameters = result.parameters;

        /*console.log(result);
        console.log('Dialogflow Score: ', score);
        console.log('Dialogflow Response Data: ', responseData);
        console.log('Dialogflow Action: ', action);
        console.log('Dialogflow Messages: ', messages);
        console.log('Dialogflow Contexts: ', contexts);
        console.log('Dialogflow Parameters: ', parameters);*/

        console.log("Result", result);
        if (result && score > 0.6) {
            // Handle response with acceptable score
            let responses = [];
            if (messages && messages.length) {
                for (var i = 0; i < messages.length; i ++) {
                    // https://dialogflow.com/docs/reference/message-objects
                    let msg = messages[i];
                    console.log('Msg '+i+': ', msg.type);
                    switch (msg.type) {
                        case 0: // text message
                            if (msg.speech) {
                                // @todo Build message
                                console.log('Message Text: ', msg.speech);
                                responses.push({
                                    type: "text",
                                    message: msg.speech
                                });
                            }

                            break;
                        case 1: // Facebook card message
                            // @todo Build message

                            break;
                        case 2: // Facebook quick replies message
                            console.log('Quick Replies: ', msg);
                            if (msg.replies && msg.replies.length) {

                            }

                            break;
                        case 3: // Facebook image message
                            if (msg.imageUrl) {

                            }

                            break;
                        case 4: // custom payload message
                            console.log('Custom payload: ', msg);
                            if (msg.payload && msg.payload.length) {
                                /*if (msg.platform === 'facebook') {

                                }*/
                                responses.push({
                                    type: "payload",
                                    message: msg.speech
                                });
                            }

                            break;
                        default:
                            break;
                    }
                }
            }

            /*if (action && action.length) {
                console.log('Action found: '+action);
            }

            if (Array.isArray(contexts) && contexts.length) {
                console.log('Contexts found: '+contexts);
            }

            if (parameters && parameters.length) {
                console.log('Parameters found: '+parameters);
            }*/

            callback(null, "Result found.");
        } else {
            callback(null, "No result found.");
        }
    });

    // Handle any errors
    request.on('error', function(error) {
        console.log('Dialogflow Error:');
        console.log(error);
        // Handle error
        callback(true, null);
    });

    // End the request
    request.end();
}
