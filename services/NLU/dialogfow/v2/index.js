const dialogflow = require('dialogflow');

const structjson = require('./structjson.js');
const nlpConfig = require('../../../config/nlp');

const projectId = nlpConfig.dialogflow.projectId;

module.exports = {

    detectTextIntent: function(sessionId, query, languageCode, callback) {
        // Instantiate a session client
        const sessionClient = new dialogflow.SessionsClient();

        if (! query || ! query.length) {

            return;
        }

        // The path to identify the agent that owns the created intent.
        const sessionPath = sessionClient.sessionPath(projectId, sessionId);

        // The text query request.
        const request = {
            session:    sessionPath,
            queryInput: {
                text: {
                    text:         query,
                    languageCode: languageCode,
                },
            },
        };

        // Detect the intent of the queries.
        new Promise(function (resolve, reject) {
            console.log(`Sending query "${query}"`);
            let results = sessionClient.detectIntent(request);
            
            resolve(results);
        }).then(function(results) {
            console.log('Detected intent');
            const response = results[0];
            console.log('Response: ', response);
            logQueryResult(sessionClient, response.queryResult);
        }).catch(function (error) {
            console.error(error);
        });
        
        
        return;
        let promise;
        if (! promise) {
            // First query.
            console.log(`Sending query "${query}"`);
            promise = sessionClient.detectIntent(request);
        } else {
            promise = promise.then(responses => {
                console.log('Detected intent');
                const response = responses[0];
                logQueryResult(sessionClient, response.queryResult);

                // Use output contexts as input contexts for the next query.
                response.queryResult.outputContexts.forEach(context => {
                    // There is a bug in gRPC that the returned google.protobuf.Struct
                    // value contains fields with value of null, which causes error
                    // when encoding it back. Converting to JSON and back to proto
                    // removes those values.
                    context.parameters = structjson.jsonToStructProto(
                        structjson.structProtoToJson(context.parameters)
                    );
                });
                request.queryParams = {
                    contexts: response.queryResult.outputContexts,
                };

                console.log(`Sending query "${query}"`);
                return sessionClient.detectIntent(request);
            });
        }

        promise.then(responses => {
                console.log('Detected intent');
                logQueryResult(sessionClient, responses[0].queryResult);
            })
            .catch(err => {
                console.error('ERROR:', err);
            });

        // [END dialogflow_detect_intent_text]
    }

}

function logQueryResult(sessionClient, result) {
    // Instantiate a context client
    const contextClient = new dialogflow.ContextsClient();

    console.log(`Query: ${result.queryText}`);
    console.log(`Response: ${result.fulfillmentText}`);
    if (result.intent) {
        console.log(`Intent: ${result.intent.displayName}`);
    } else {
        console.log(`No intent matched.`);
    }
    const parameters = JSON.stringify(
        structjson.structProtoToJson(result.parameters)
    );
    console.log(`Parameters: ${parameters}`);
    if (result.outputContexts && result.outputContexts.length) {
        console.log(`Output contexts:`);
        result.outputContexts.forEach(context => {
            const contextId = contextClient.matchContextFromContextName(context.name);
            const contextParameters = JSON.stringify(
                structjson.structProtoToJson(context.parameters)
            );
            console.log(`${contextId}`);
            console.log(`lifespan: ${context.lifespanCount}`);
            console.log(`parameters: ${contextParameters}`);
        });
    }
}
