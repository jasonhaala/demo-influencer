'use strict'

const rp = require('request-promise');
const querystring = require('querystring');

const nlpConfig = require('../../../config');

// Load the configs required to enable LUIS
const LuisEndpointUrl = nlpConfig.LUIS.endpointUrl;

// Determine if the LUIS configs have been set to enable it
const isEnabled = LuisEndpointUrl ? true : false;

module.exports = {

    isEnabled: isEnabled,

    /**
     *
     * @param {string} matchingType // Return all matching intents or only the top match (top|all)
     * @param session
     * @param args
     * @param next
     * @constructor
     */
    LuisMatching: function(matchingType, session, args, next) {
        let utterance = session.message.text;
        let nextArgs = {utterance: utterance};

        if (! isEnabled) {
            console.error('Skipping LUIS matching. Missing configs.');
            next();
        } else {
            if (matchingType.toLowerCase() === 'top') {
                console.log('Checking LUIS for top intent...');
                matchingType = 'top';
            } else {
                console.log('Checking LUIS for all matching intents...');
                matchingType = 'all';
            }

            // Check LUIS for a matching Intent
            getLuisIntents(matchingType, utterance, function(error, intents) {
                if (error) {
                    // Error during the request
                    console.error(error);
                    next(nextArgs);
                } else {
                    for (let i = 0, count = intents.length; i < count; i ++) {
                        let intent = intents[i].intent;
                        let score = intents[i].score;
                        if (intent.intent === 'None' || score < 70) {
                            next(nextArgs);
                        } else {
                            // Found matching intent
                            try {
                                // Attempt to load the matching intent's Dialog
                                session.endDialog();
                                session.beginDialog(intent);
                            } catch (error) {
                                console.error(error);
                                next(nextArgs);
                            }
                        }
                    }
                }
            });
        }
    },

    /**
     * Returns all LUIS intents matching an utterance
     * @param {string} utterance
     * @param {function} callback
     * @returns {null|string|obj}
     */
    getAllMatchingLuisIntents: function(utterance, callback) {
        getLuisIntents('all', utterance, function(error, result) {

            callback(error, result);
        });
    },

    /**
     * Returns the top LUIS intent matching an utterance
     * @param {string} utterance
     * @param {function} callback
     * @returns {null|string|obj}
     */
    getTopLuisIntent: function(utterance, callback) {
        getLuisIntents('top', utterance, function(error, result) {

            callback(error, result);
        });
    }

}


/**
 * Sends a GET request to the LUIS service to find matching intents for an utterance
 * @param {string} matchingType // Return all matching intents or only the top match (top|all)
 * @param {string} utterance
 * @param {function} callback
 * @returns {null|string|obj}
 */
function getLuisIntents(matchingType, utterance, callback) {
    if (! isEnabled) {
        var errorMessage = 'Missing LUIS Endpoint URL configuration value.';
        console.error(errorMessage);
        callback(errorMessage, null);

        return;
    }

    var options = {
        method: 'GET',
        uri: LuisEndpointUrl + querystring.escape(utterance),
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true // Automatically stringifies the body to JSON
    }

    // Set the top matching intent. A value will be set by the API response.
    rp(options).then(function (body) {
        //console.log(body);
        //console.log(`Top Intent: ${data.topScoringIntent.intent}`);
        //console.log('Intents:');
        //console.log(body.intents);
        // Check for the top scoring intent
        var type = matchingType.toLowerCase();
        if (! body.query) {
            // Empty query. Nothing to return
            callback(null, null);
            return;
        } else if (type === 'all' && body.intents) {
            // Return all the matching intents
            callback(null, body.intents);
            return;
        } else if (body.topScoringIntent.intent && body.topScoringIntent.intent !== 'None') {
            // Return the top scoring intent
            callback(null, body.topScoringIntent.intent);
            return;
        } else {
            // Nothing found to return
            callback(null, null);
            return;
        }
    }).catch(function (error){
        console.error(error.message);
        callback(error.message, null);
        return;
    }).finally(function () {
        //
    });
}
