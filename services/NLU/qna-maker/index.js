'use strict'

const rp = require('request-promise');
const querystring = require('querystring');

const nlpConfig = require('../../../config');

// Load the configs required to enable QnA Maker
const knowledgeBaseId = nlpConfig.qnaMaker.knowledgeBaseId;
const subscriptionKey = nlpConfig.qnaMaker.subscriptionKey;

// Determine if the QnA Maker configs have been set to enable it
const isEnabled = knowledgeBaseId && subscriptionKey ? true : false;

module.exports = {

    isEnabled: isEnabled,

    qnaMatching: function(session, args, next) {
        // Check QnA for an answer
        console.log('Checking QnA...');
        module.exports.getQnAAnswer(args.utterance, function(error, result) {
            if (error) {
                // Error during the request
                console.log('An error occurred during QnA request.');
                next(args);
            } else if (! result.answer) {
                // No errors, but no matching answer found
                console.log('No matching QnA answer found.');
                next(args);
            } else {
                // Found matching answer
                console.log('Found matching QnA answer.')
                let hasAnswer = result.answer === "No good match found in the KB" ? false : true;
                if (! hasAnswer || result.score < 65) {
                    console.log('QnA answer missing or too low score.');
                    next(args);
                } else {
                    console.log('QnA answer determined to be correct.');
                    try {
                        session.endDialog();
                        session.beginDialog(result.answer);
                    } catch (error) {
                        console.error(error.message);
                        next(args);
                    }
                }
            }
        });
    },

    getQnAAnswer: function(utterance, callback) {
        // Check if required access variables are set
        if (! isEnabled) {
            let errorMessage = 'Missing QnA KnowledgeBase configuration values.';
            callback(errorMessage, null);
        } else {
            let url = 'https://westus.api.cognitive.microsoft.com/qnamaker/v1.0/knowledgebases/'+ knowledgeBaseId +'/generateAnswer';
            let options = {
                method: 'POST',
                uri: url,
                headers: {
                    'Ocp-Apim-Subscription-Key': subscriptionKey,
                    'Content-Type': 'application/json'
                },
                body: {
                    question: utterance
                },
                json: true  // Automatically stringifies the body to JSON
            }

            // Set the top matching intent. A value will be set by the API response.
            rp(options)
                .then(function(body) {
                    //console.log(body);
                    if (body.answer && body.score) {
                        // Answer found
                        callback(null, body);
                    } else {
                        // Nothing to return
                        callback(null, null);
                    }
                }).catch(function(error) {
                    console.error(error.message);
                    callback(error.message, null);
                });/*.finally(function() {
                    //
                })*/
        }
    }

}
