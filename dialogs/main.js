const builder = require('botbuilder');

//const mongo = require('../services/mongodb');
const dialogflow = require('../services/NLU/dialogfow/v1');
const heroCardCarouselFactory = require('../factory/herocard-carousel');

const libraryName = 'main';
const libraryAction = libraryName+':/';
let dialogName;
let dialogAction;

let lib = new builder.Library(libraryName);


dialogName = 'test';
dialogAction = libraryAction + dialogName;
lib.dialog('/'+dialogName, [
    function(session) {
        let sessionId = '3bc3cc87-b57d-4c61-b2b3-2b14af4a5434';
        let msgText = 'who are you?';
        let languageCode = 'en-US';
        dialogflow.textRequest(session, function(error, messages) {
            if (error) {
                botlog.error(error);
                session.endDialog("I wasn't able to get the intent from Dialogflow.");
            } else {
                let msg = "Dialogflow success!";

                session.endDialog(msg);
            }
        });
    }
]).triggerAction({
    matches: dialogAction
});


dialogName = 'fallback';
dialogAction = libraryAction + dialogName;
lib.dialog('/'+dialogName, [
    /*function(session, args, next) {
        /!**
         * LUIS
         *!/
        if (! LUIS.isEnabled) {
            botlog.debug('Skipping LUIS matching...');
            next();
        } else {
            //LuisMatching(session, args, next);
        }
    },*/

    /**
     * Dialogflow NLU
     */
    function(session, args, next) {
        if (! dialogflow) {
            botlog.debug('Skipping Dialogflow matching...');
            next();
        } else {
            dialogflow.textRequest(session, function(error, messages) {
                if (error) {
                    botlog.error(error);
                    session.endDialog("I wasn't able to get the intent from Dialogflow.");
                } else if (messages && messages.length) {
                    let msg;
                    const endCount = messages.length;
                    for (var i = 0; i < messages.length; i++) {
                        msg = messages[i];
                        session.send(msg);
                        //if ()
                    }
                } else {
                    botlog.debug(`No matching results from Dialogflow.`);
                    next();
                }
            });
        }
    },

    /*function(session, args, next) {
        /!**
         * QnA Maker
         *!/
        if (! QnAMaker.isEnabled) {
            botlog.debug('Skipping QnAMaker matching...');
            next();
        } else {
            qnaMatching(session, args, next);
        }
    },*/

    function(session, args) {
        //
        /**
         * Final Fallback response
         */

        // @todo Log the fallback to the database to be handled or trained.

        let msg = [
            `Sorry, I'm not able to understand that yet. I'm still learning to do complex things like understand human language.`
        ]
        /*let msg = new builder.Message(session)
            .text(`Sorry, I'm not able to understand that yet. I'm still learning to do complex things like understand human language.`)
            .suggestedActions(
                builder.SuggestedActions.create(
                    session, [
                        builder.CardAction.dialogAction(session, "menu:/").title("Go To Main Menu"),
                        builder.CardAction.dialogAction(session, "shop:/").title("Shop For Gifts"),
                        builder.CardAction.dialogAction(session, "support:/").title("Contact Support"),
                        builder.CardAction.dialogAction(session, "faq:/").title("Get Answers")
                    ]
                ));*/

        session.endDialog(msg);
    },
]).triggerAction({
    matches: dialogAction
});


dialogName = 'welcome';
dialogAction = libraryAction + dialogName;
lib.dialog('/'+dialogName, [
    function (session, args, next) {
        let msg = [
            `Welcome to the bot! This is the "Get Started" welcome message.`
        ];
        session.send(msg).endDialog();
        session.beginDialog('main:/features');
    }
]).triggerAction({
    matches: dialogAction
});


dialogName = 'features';
dialogAction = libraryAction + dialogName;
lib.dialog('/'+dialogName, [
    function (session, args, next) {
        let cardItems = [
            {
                title: "Music",
                subtitle: "Card subtitle",
                text: "Card description text.",
                images: [
                    {
                        url: "https://icdn6.digitaltrends.com/image/google-deepmind-artificial-intelligence-2-720x720.jpg"
                    }
                ],
                buttons: [
                    {
                        type: "dialogAction",
                        title: "Show All Music",
                        value: "main:/music",
                    },
                    {
                        type: "dialogAction",
                        title: "Watch Videos",
                        value: "videos:/",
                    },
                    {
                        type: "dialogAction",
                        title: "Listen to Songs",
                        value: "audio:/",
                    }
                ]
            },
            {
                title: "Events",
                subtitle: "Card subtitle",
                text: "Card description text.",
                images: [
                    {
                        url: "https://icdn6.digitaltrends.com/image/google-deepmind-artificial-intelligence-2-720x720.jpg"
                    }
                ],
                buttons: [
                    {
                        type: "dialogAction",
                        title: "See Upcoming Events",
                        value: "events:/",
                    }
                ]
            },
            {
                title: "Merch",
                subtitle: "Card subtitle",
                text: "Card description text.",
                images: [
                    {
                        url: "https://icdn6.digitaltrends.com/image/google-deepmind-artificial-intelligence-2-720x720.jpg"
                    }
                ],
                buttons: [
                    {
                        type: "dialogAction",
                        title: "Shop the Goods",
                        value: "shop:/",
                    }
                ]
            },
            {
                title: "About Me",
                subtitle: "Card subtitle",
                text: "Card description text.",
                images: [
                    {
                        url: "https://icdn6.digitaltrends.com/image/google-deepmind-artificial-intelligence-2-720x720.jpg"
                    }
                ],
                buttons: [
                    {
                        type: "dialogAction",
                        title: "Frequently Asked Questions",
                        value: "faq:/",
                    }
                ]
            },
            {
                title: "Merch",
                subtitle: "Card subtitle",
                text: "Card description text.",
                images: [
                    {
                        url: "https://icdn6.digitaltrends.com/image/google-deepmind-artificial-intelligence-2-720x720.jpg"
                    }
                ],
                buttons: [
                    {
                        type: "dialogAction",
                        title: "Shop the Goods",
                        value: "shop:/",
                    }
                ]
            },
            {
                title: "Connect",
                subtitle: "Card subtitle",
                text: "Card description text.",
                images: [
                    {
                        url: "https://icdn6.digitaltrends.com/image/google-deepmind-artificial-intelligence-2-720x720.jpg"
                    }
                ],
                buttons: [
                    {
                        type: "openUrl",
                        title: "Follow on Instagram",
                        value: "https://twitter.com",
                    },
                    {
                        type: "openUrl",
                        title: "Follow on Twitter",
                        value: "https://instagram.com",
                    },
                    {
                        type: "openUrl",
                        title: "Visit My Website",
                        value: "https://google.com",
                    }
                ]
            }
        ];

        heroCardCarouselFactory(session, cardItems, function(error, msg) {
            if (error) {
                botlog.error(error);

                let errorMsg = `I seem to be having a problem loading the main menu right now.`;
                session.endDialog(errorMsg);
            } else {
                session.send(`Main Menu:`);

                session.endDialog(msg);
            }
        });
    }
]).triggerAction({
    matches: dialogAction
});


dialogName = 'music';
dialogAction = libraryAction + dialogName;
lib.dialog('/'+dialogName, [
    function(session) {
        let cardItems = [
            {
                title: "Song #1",
                subtitle: "Song subtitle",
                text: "Song description text.",
                images: [
                    {
                        url: "https://icdn6.digitaltrends.com/image/google-deepmind-artificial-intelligence-2-720x720.jpg"
                    }
                ],
                buttons: [
                    {
                        type: "dialogAction",
                        title: "Play Song",
                        value: "audio:/song1",
                    },
                    {
                        type: "dialogAction",
                        title: "Watch Video",
                        value: "videos:/song1",
                    }
                ]
            },
            {
                title: "Song #2",
                subtitle: "Song subtitle",
                text: "Song description text.",
                images: [
                    {
                        url: "https://icdn6.digitaltrends.com/image/google-deepmind-artificial-intelligence-2-720x720.jpg"
                    }
                ],
                buttons: [
                    {
                        type: "dialogAction",
                        title: "Play Song",
                        value: "audio:/son1",
                    },
                    {
                        type: "dialogAction",
                        title: "Watch Video",
                        value: "videos:/song1",
                    }
                ]
            }
        ];

        heroCardCarouselFactory(session, cardItems, function(error, msg) {
            if (error) {
                botlog.error(error);

                let errorMsg = `I seem to be having a problem loading the main menu right now.`;
                session.endDialog(errorMsg);
            } else {
                session.send(`Main Menu:`);

                session.endDialog(msg);
            }
        });
    }
]).triggerAction({
    matches: dialogAction
});


module.exports.createLibrary = function () {
    return lib.clone();
};
