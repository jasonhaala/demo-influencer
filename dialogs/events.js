const builder = require('botbuilder');
//const mongo = require('../services/mongodb');

const libraryName = 'events';
const libraryAction = libraryName+':/';
let dialogName;
let dialogAction;

let lib = new builder.Library(libraryName);

dialogName = '';
dialogAction = libraryAction + dialogName;
lib.dialog('/'+dialogName, [
    function(session) {
        let msg = [
            `This is the events module.`
        ];

        session.endDialog(msg);
    }
]).triggerAction({
    matches: dialogAction
});



module.exports.createLibrary = function () {
    return lib.clone();
};
