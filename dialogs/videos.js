const builder = require('botbuilder');
//const mongo = require('../services/mongodb');

const libraryName = 'videos';
const libraryAction = libraryName + ':/';
let dialogName;
let dialogAction;

let lib = new builder.Library(libraryName);

dialogName = '';
dialogAction = libraryAction + dialogName;
lib.dialog('/' + dialogName, [
    function(session) {
        let msg = [
            `This is the Videos module.`
        ];

        session.endDialog(msg);
    }
]).triggerAction({
    matches: dialogAction
});


dialogName = 'song1';
dialogAction = libraryAction + dialogName;
lib.dialog('/' + dialogName, [
    function(session) {
        /*let msgResponses = [
         `You're listening to "Song #1"`
         ];

         let msgText = msgResponses[Math.floor(Math.random() * msgResponses.length)];*/

        let videoData = {
            //url: "http://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4",
            //url: "https://www.facebook.com/788760788001802/videos/789978791213335/",
            url: "https://www.youtube.com/watch?v=aqz-KE-bpKQ"
        };

        let msg = buildVideoMessage(session, videoData)

        session.endDialog(msg);
    }
]).triggerAction({
    matches: dialogAction
});


/**
 * Builds the channel-specific video message
 * @param session
 * @param {object} videoData
 */
function buildVideoMessage(session, videoData, buttons) {
    let msg;
    if (session.message.source === "facebook") {
        msg = new builder.Message(session).sourceEvent({
            facebook: {
                attachment: {
                    type: "template",
                    payload: {
                        template_type: "media",
                        elements: [
                            {
                                media_type: "video",
                                url: videoData.url
                            }
                        ]
                    }
                }
            }
        });
    } else {
        msg = new builder.Message(session)
            .addAttachment(
                new builder.VideoCard(session)
                    .title('Big Buck Bunny')
                    .subtitle('by the Blender Institute')
                    .text('Big Buck Bunny (code-named Peach) is a short computer-animated comedy film by the Blender Institute, part of the Blender Foundation. Like the foundation\'s previous film Elephants Dream, the film was made using Blender, a free software application for animation made by the same foundation. It was released as an open-source film under Creative Commons License Attribution 3.0.')
                    .image(builder.CardImage.create(session, 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Big_buck_bunny_poster_big.jpg/220px-Big_buck_bunny_poster_big.jpg'))
                    .media([
                        { url: videoData.url }
                    ])
                    .buttons([
                        builder.CardAction.openUrl(session, 'main:/music', 'Watch more videos'),
                        builder.CardAction.openUrl(session, 'main:/music', 'See all music'),
                        builder.CardAction.openUrl(session, 'main:/features', 'Main menu')
                    ])
                    .shareable(true)
                    .autostart(true)
                    .autoloop(true)
            );
    }

    return msg;
}

module.exports.createLibrary = function() {
    return lib.clone();
};

