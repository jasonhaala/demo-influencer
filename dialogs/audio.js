const builder = require('botbuilder');
//const mongo = require('../services/mongodb');

const libraryName = 'audio';
const libraryAction = libraryName+':/';
let dialogName;
let dialogAction;

let lib = new builder.Library(libraryName);

dialogName = '';
dialogAction = libraryAction + dialogName;
lib.dialog('/' + dialogName, [
    function(session) {
        let msg = [
            `This is the Audio module.`
        ];

        session.endDialog(msg);
    }
]).triggerAction({
    matches: dialogAction
});


dialogName = 'song1';
dialogAction = libraryAction + dialogName;
lib.dialog('/'+dialogName, [
    function(session) {
        /*let msgResponses = [
         `You're listening to "Song #1"`
         ];

         let msgText = msgResponses[Math.floor(Math.random() * msgResponses.length)];*/

        let audioData = {
            url: "http://www.wavlist.com/movies/004/father.wav"
        };

        let msg = buildAudioMessage(session, audioData);

        session.endDialog(msg);
    }
]).triggerAction({
    matches: dialogAction
});


/**
 * Builds the channel-specific video message
 * @param session
 * @param {object} videoData
 */
function buildAudioMessage(session, audioData, buttons) {
    let msg;
    if (session.message.source === "facebook") {
        msg = new builder.Message(session)
            .sourceEvent({
                facebook: {
                    'type':'audio',
                    'payload': {
                        'url': audioData.url,
                        'is_reusable': true
                    }
                }
            });
    } else {
        msg = new builder.Message(session)
            .addAttachment(
                new builder.AudioCard(session)
                    .title('I am your father')
                    .subtitle('Star Wars: Episode V - The Empire Strikes Back')
                    .text('The Empire Strikes Back (also known as Star Wars: Episode V – The Empire Strikes Back) is a 1980 American epic space opera film directed by Irvin Kershner. Leigh Brackett and Lawrence Kasdan wrote the screenplay, with George Lucas writing the film\'s story and serving as executive producer. The second installment in the original Star Wars trilogy, it was produced by Gary Kurtz for Lucasfilm Ltd. and stars Mark Hamill, Harrison Ford, Carrie Fisher, Billy Dee Williams, Anthony Daniels, David Prowse, Kenny Baker, Peter Mayhew and Frank Oz.')
                    .image(builder.CardImage.create(session, 'https://upload.wikimedia.org/wikipedia/en/3/3c/SW_-_Empire_Strikes_Back.jpg'))
                    .media([
                        { url: audioData.url }
                    ])
                    .buttons([
                        builder.CardAction.openUrl(session, 'https://en.wikipedia.org/wiki/The_Empire_Strikes_Back', 'Read More')
                    ])
                    .shareable(true)
                    .autostart(true)
                    .autoloop(true)
            );
    }

    return msg;
}



module.exports.createLibrary = function () {
    return lib.clone();
};
