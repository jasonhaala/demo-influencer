'use strict'

const app = require('./app');
const storage = require('./storage');
const azure = require('./azure');
const analytics = require('./analytics');
const channels = require('./channels');
const nlp = require('./nlp');

module.exports = {
    app:       app,
    azure:     azure,
    storage:   storage,
    analytics: analytics,
    channels:  channels,
    nlp:       nlp
}

