'use strict'

/*
 |--------------------------------------------------------------------------
 | Analytics Services
 |--------------------------------------------------------------------------
 |
 */
module.exports = {

    chatbase: {
        apiKey: process.env.ChatbaseApiKey || null,
    },

    botmetrics: {
        apiKey: process.env.BotmetricsApiKey || null,
        botId: process.env.BotmetricsBotId || null,
    },

    /*dashbot: {
    },*/

    /*botanalytics: {

    },*/

    /*yandex: {

     },*/

}
