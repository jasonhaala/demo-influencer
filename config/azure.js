'use strict'

/*
 |--------------------------------------------------------------------------
 | Microsoft Azure
 |--------------------------------------------------------------------------
 |
 */
module.exports = {

    microsoft: {
        appId: process.env.MicrosoftAppId || null,
        appPassword: process.env.MicrosoftAppPassword || null,
    },

}
