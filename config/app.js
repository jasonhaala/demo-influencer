'use strict'

module.exports = {

    /*
     |--------------------------------------------------------------------------
     | Application Environment
     |--------------------------------------------------------------------------
     |
     | This value determines the "environment" your application is currently
     | running in. This may determine how you prefer to configure various
     | services your application utilizes.
     |
     */
    env: process.env.BotEnv || 'production',

    /*
     |--------------------------------------------------------------------------
     | Application Debug Mode
     |--------------------------------------------------------------------------
     |
     | When the application is in debug mode, detailed messages will be logged
     | to the console.
     |
     */
    debug: process.env.Debug || false,

    /*
     |--------------------------------------------------------------------------
     | Application URL
     |--------------------------------------------------------------------------
     |
     */
    url: process.env.AppUrl || 'http://localhost',

    /*
     |--------------------------------------------------------------------------
     | Application Server Port
     |--------------------------------------------------------------------------
     |
     */
    port: process.env.PORT || '3978',

}
