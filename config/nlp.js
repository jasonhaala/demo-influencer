'use strict'

/*
 |--------------------------------------------------------------------------
 | Natural Language Processing Services
 |--------------------------------------------------------------------------
 |
 */
module.exports = {

    LUIS: {
        endpointUrl: process.env.LuisEndpointUrl || null,
    },

    qnaMaker: {
        knowledgeBaseId: process.env.QnAKnowledgeBaseId || null,
        subscriptionKey: process.env.QnASubscriptionKey || null
    },

    dialogflow: {
        //projectId: process.env.DialogflowProjectId || null,
        token: process.env.DialogflowClientAccessToken || null
    }

}
