'use strict'

/*
 |--------------------------------------------------------------------------
 | Messaging Platforms
 |--------------------------------------------------------------------------
 |
 */
module.exports = {

    facebook: {
        pageAccessToken: process.env.FacebookPageAccessToken || null,
    },

    /*slack: {

     },*/

    /*twilio: {

     },*/

    /*kik: {

     },*/

}
