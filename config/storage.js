'use strict'

module.exports = {

    /*
     |--------------------------------------------------------------------------
     | State Data Storage
     |--------------------------------------------------------------------------
     |
     | Supported: "memory", "azuretable", "azurecosmos", "mongodb"
     |
     | Important: Use memory storage for local testing and prototyping only.
     |
     */
    stateData: {

        default: process.env.StateDataDefault || 'memory',

        azurecosmos: {
            masterKey:  process.env.AzureCosmosMasterKey || null,
            host:       process.env.AzureCosmosHost || null,
            database:   process.env.AzureCosmosDatabase || null,
            collection: process.env.AzureCosmosCollection || null,
        },

        azuretable: {
            key:   process.env.AzureTableStorageKey || null,
            name:  process.env.AzureTableStorageName || null,
            table: process.env.AzureTableStorageTable || null
        },

    },

    /*
     |--------------------------------------------------------------------------
     | Databases
     |--------------------------------------------------------------------------
     |
     */
    database: {

        /*mysql: {
         driver: 'mysql',
         host: process.env.MySQLHost || '127.0.0.1',
         port: process.env.MySQLPort || '3306',
         database: process.env.MySQLDatabase || 'test',
         username: process.env.MySQLUsername || 'root',
         password: process.env.MySQLPassword || '',
         },*/

        mongodb: {
            ssh: {
                host: process.env.MongoDbSSHHost || null,
                port: process.env.MongoDbSSHPort || null,
                user: process.env.MongoDbSSHUser || null,
                password: process.env.MongoDbSSHPassword || null,
                privateKey: process.env.MongoDbSSHPrivateKeyFile || null,
            },
            host: process.env.MongoDbHost || '127.0.0.1',
            port: process.env.MongoDbPort || '27017',
            database: process.env.MongoDbDatabase || null,
            uniqueLocalPort: process.env.MongoDBUniquePort || 28349
        }

    }

}
