const mongoose = require('mongoose');

const ModelSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    firstName: String,
    lastName:  String,
    email:     String,
    phone:     String
}, {
    collection: 'Contacts'
});

module.exports = mongoose.model('Contact', ModelSchema);
