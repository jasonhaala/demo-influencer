'use strict';

var fs = require('fs');
var tunnel = require('tunnel-ssh');
const mongoose = require('mongoose');

const storageConfig = require('../../config/storage');

let Schema = mongoose.Schema;

module.exports = {

    connect: function() {

        return connection();
    },

}


/**
 * Mongoose Model
 *
 * @param {string} modelName
 * @param {Schema} modelSchema
 */
function buildModel(modelName, modelSchema) {
    //connection();

    return mongoose.model(modelName, modelSchema);
}

function connection() {
    console.log("Connection...");
    const dbHost = storageConfig.database.mongodb.host;
    const dbPort = storageConfig.database.mongodb.port;
    const dbName = storageConfig.database.mongodb.database;

    if (! (dbHost && dbPort && dbName)) {
        console.error(`Required MongoDB config value(s) are missing.`);

        return null;
    }

    // Use unique port to prevent conflicts with other server apps using MongoDB
    const uniqueLocalPort = 28349;

    const sshHost = storageConfig.database.mongodb.ssh.host;
    const sshPort = storageConfig.database.mongodb.ssh.port;
    const sshUser = storageConfig.database.mongodb.ssh.user;
    const sshPassword = storageConfig.database.mongodb.ssh.password;
    const sshPrivateKey = storageConfig.database.mongodb.ssh.privateKey;

    if (! (sshHost && sshPort && sshUser && (sshPassword || sshPrivateKey))) {
        console.error(`Missing SSH tunnel config values for MongoDB connection. Bypassing secure tunnel.`);

        return mongoose.connect('mongodb://'+ dbHost +':'+ uniqueLocalPort, {
            dbName: dbName
        });
    } else {
        console.log("Using SSH");
        const tunnelConfig = {
            host: sshHost,
            port: sshPort,
            username: sshUser,
            dstHost: dbHost,
            dstPort: dbPort,
            localPort: uniqueLocalPort,
            localHost: '127.0.0.1',
        };
        if (sshPassword) {
            tunnelConfig.password = sshPassword;
        } else {
            tunnelConfig.privateKey = fs.readFileSync('./keys/'+sshPrivateKey);
        }

        tunnel(tunnelConfig, function (error, server) {
            if (error) {
                console.error("SSH connection error: " + error);
            }

            const mongooseConfig = {
                dbName: dbName,
                autoIndex: false, // Don't build indexes (index builds can cause performance degradation)
                autoReconnect: true,
                reconnectTries: 10,
                reconnectInterval: 500, // Reconnect every 500ms
                poolSize: 5, // # of socket connections to keep open
                // If not connected, return errors immediately rather than waiting for reconnect
                bufferMaxEntries: 0,
                keepAlive: 120
            };

            //return mongoose.connect('mongodb://'+ dbHost +':'+ dbPort, mongooseConfig);

            mongoose.connect('mongodb://'+ dbHost +':'+ uniqueLocalPort, mongooseConfig).then(
                () => {
                    /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
                    console.log("successful MongoDB connection.");
                },
                err => {
                    /** handle initial connection error */
                    console.error(err);
                }
            );

            /*mongoose.connect('mongodb://'+ dbHost +':'+ dbPort, mongoOptions);

            const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'DB connection error:'));
            db.once('open', function() {
                // we're connected!
                console.log("DB connection successful");
                // console.log(server);
            });*/
        });
    }
}
