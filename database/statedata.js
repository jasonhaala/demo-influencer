'use strict';

const builder = require('botbuilder');
const azure = require('botbuilder-azure');
const MongoClient = require('mongodb'); // v3.x.x
const MongoBotStorage = require('botbuilder-storage');

const storageConfig = require('../config/storage');

let defaultStorage = storageConfig.stateData.default;
if (! defaultStorage) {
    botlog.error('Missing default State Data Storage config value.');
    defaultStorage = null;
} else {
    defaultStorage.toLowerCase();
}

module.exports = function() {
    switch (defaultStorage) {
        case 'azuretable':
            return azureTableStorage();
        case 'azurecosmos':
            return azureCosmosStorage();
        case 'mongodb':
            return mongoDBStorage();
        default:
            return inMemory();
    }
};


function inMemory() {
    botlog.debug('Using In-Memory State Data');

    return new builder.MemoryBotStorage();
}

function azureCosmosStorage() {
    const host = storageConfig.stateData.azurecosmos.host;
    const masterKey = storageConfig.stateData.azurecosmos.masterKey;
    const database = storageConfig.stateData.azurecosmos.database;
    const collection = storageConfig.stateData.azurecosmos.collection;

    // If any configs are missing, default to in-memory storage
    if (! (host || masterKey || database || collection)) {
        botlog.debug('Skipping Cosmos DB State Data. Missing required config value(s).');

        return inMemory();
    } else {
        botlog.debug('Using Cosmos DB State Data.');
        let documentDbOptions = {
            host:       host,
            masterKey:  masterKey,
            database:   database,
            collection: collection
        };
        const docDbClient = new azure.DocumentDbClient(documentDbOptions);

        return new azure.AzureBotStorage({gzipData: false}, docDbClient);
    }
}

function azureTableStorage() {
    const storageKey = storageConfig.stateData.azuretable.key;
    const storageName = storageConfig.stateData.azuretable.name;
    const tableName = storageConfig.stateData.azuretable.table;
    if (! (tableName || storageName || storageKey)) {
        botlog.debug('Skipping Table Storage State Data. Missing required config value(s).');

        return inMemory();
    } else {
        botlog.debug('Using Table Storage State Data.');
        const azureTableClient = new azure.AzureTableClient(tableName, storageName, storageKey);

        return new azure.AzureBotStorage({gzipData: false}, azureTableClient);
    }
}

function mongoDBStorage() {
    const dbHost = storageConfig.database.mongodb.host;
    const dbPort = storageConfig.database.mongodb.port;
    const dbName = storageConfig.database.mongodb.database;

    if (! (dbHost || dbPort || dbName)) {
        botlog.debug(`Skipping MongoDB State Data. Missing required config value(s).`);

        return inMemory();
    } else {
        // @todo Integrate redundant functionality with services/mongodb
        MongoClient.connect('mongodb://'+ dbHost +':'+ dbPort, (error, client) => {
            if (error) {
                botlog.error(error);
                throw error
            } else {
                // Define the adapter settings
                const settings = {
                    // Required. This is the collection where all the conversation state data will be saved.
                    collection: 'stateData',

                    // Optional but recommended
                    ttl: {
                        userData: 3600 * 24 * 365, // one year,
                        conversationData: 3600 * 24 * 7, // one week,
                        privateConversationData: 3600 * 24 * 7 // one week
                    }
                }
                // Select the database with the client
                client = client.db(dbName);

                // Instantiate the adapter with the client and settings.
                return new MongoBotStorage(client, settings)
            }
        });
    }
}
