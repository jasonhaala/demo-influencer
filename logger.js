'use strict';

const { createLogger, format, transports } = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');
const { combine, timestamp, label, printf } = format;
const appConfig = require('./config/app');

const logFormat = printf(info => {
    return `${info.timestamp} [${info.level.toUpperCase()}] :: ${info.message}`;
});

/*
 * Set the minimum logging level, depending on the server environment
 */
let minLevel = appConfig.debug ? 'debug' : 'info';

let logger = createLogger({
    levels: {
        emergency: 0,
        alert: 1,
        critical: 2,
        error: 3,
        warning: 4,
        info: 5,
        debug: 6
    },
    format: combine(
        //label({ label: 'Chosen Label Name' }),
        timestamp(),
        logFormat
    )
});

logger.exceptions.handle(
    new transports.File({ filename: 'storage/logs/exceptions.log' })
);

if (isProduction) {
    logger.add(new DailyRotateFile({
        level: 'error',
        filename: 'storage/logs/errors-%DATE%.log',
        datePattern: 'YYYY-MM-DD-HH',
        zippedArchive: false,
        maxSize: '20m',
        maxFiles: '7d',
        handleExceptions: true
    }));

    logger.add(new DailyRotateFile({
        level: minLevel,
        filename: 'storage/logs/combined-%DATE%.log',
        datePattern: 'YYYY-MM-DD-HH',
        zippedArchive: false,
        maxSize: '20m',
        maxFiles: '3d'
    }));
} else {
    logger.add(new transports.Console({
        level: minLevel
    }));
}

module.exports = logger;
