'use strict';

const builder = require('botbuilder');

const buttonFactory = require('../factory/button');

module.exports = function(session, cardItems, callback) {
    let msg = new builder.Message(session).attachmentLayout('carousel');
    let attachments = [];

    new Promise(function(resolve, reject) {
        botlog.debug('Building card carousel..');
        const itemCount = cardItems.length;
        const finalCount = itemCount - 1;
        let currentCount = 0;
        for (var i = 0; i < itemCount; i ++) {
            let cardItem = cardItems[i];

            let carouselCard = new builder.HeroCard(session)
                .title(cardItem.title)
                .text(cardItem.text);

            let images = [];
            let buttons = [];

            // Add the images
            new Promise(function (resolve, reject) {
                const imageCount = cardItem.images.length;
                if (imageCount === 0) {
                    resolve();
                } else {
                    let key = 0;
                    while (cardItem.images[key]) {
                        if (key < imageCount) {
                            let imageData = cardItem.images[key];
                            let image = builder.CardImage.create(session, imageData.url);

                            images.push(image);

                            if (key === imageCount - 1) {
                                carouselCard.images(images);
                                resolve();
                            }

                            key++;
                        }
                    }
                }
            }).then(function() {
                // Add the buttons
                return new Promise(function (resolve, reject) {
                    const buttonCount = cardItem.buttons.length;
                    if (buttonCount === 0) {
                        resolve();
                    } else {
                        let key = 0;
                        while (cardItem.buttons[key]) {
                            if (key <= buttonCount) {
                                let btnData = cardItem.buttons[key];
                                buttonFactory.build(session, btnData.type, btnData, function(error, button) {
                                    buttons.push(button)

                                    if (key === buttonCount - 1) {
                                        carouselCard.buttons(buttons);
                                        resolve();
                                    }
                                });

                                key++;
                            }
                        }
                    }
                });
            }).then(function() {
                // Attach the carousel card to the message
                attachments.push(carouselCard);

                if (currentCount === finalCount) {
                    resolve(attachments);
                } else {
                    currentCount ++;
                }
            }).catch(function (error) {
                console.error(error);
            });
        }
    }).then(function(attachments) {
        msg.attachments(attachments);
        callback(null, msg);
    }).catch(function(error) {
        console.error(error);
        callback(error, null);
    });
}
