'use strict';

const builder = require('botbuilder');

/**
 * Message Template Builder
 * Wrapper class for sending cards
 * https://docs.microsoft.com/en-us/bot-framework/nodejs/bot-builder-nodejs-message-create
 */

module.exports = {

    // The Adaptive Card can contain any combination of text, speech, images, buttons, and input fields. Adaptive Cards are created using the JSON format specified in the Adaptive Cards schema, which gives you full control over card content and format.
    // The aesthetics of the card are adapted to the channel's look and feel, making it feel native to the app and familiar to the user. You can use the Adaptive Cards' Visualizer to see how your card renders on different channels.
    adaptiveCard: function(session) {

    },

    /**
     * https://docs.microsoft.com/en-us/bot-framework/dotnet/bot-builder-dotnet-channeldata
     *
     * @param session
     * @param {string} channel
     * @param {object} channelPayload
     */
    channelSpecificMessage: function(session, channel, attachment) {
        let msg = new botbuilder.Message(session).sourceEvent({
            //specify the channel and it's specific attachment object
            [channel]: {
                attachment: attachment //format according to channel's requirements
            }
        });
    },

    /**
     * Suggested actions enable your bot to present buttons that the user can tap to provide input. Suggested actions
     * appear close to the composer and enhance user experience by enabling the user to answer a question or make a
     * selection with a simple tap of a button, rather than having to type a response with a keyboard.
     *
     * Unlike buttons that appear within rich cards (which remain visible and accessible to the user even after being
     * tapped), buttons that appear within the suggested actions pane will disappear after the user makes a selection.
     * This prevents the user from tapping stale buttons within a conversation and simplifies bot development (since
     * you will not need to account for that scenario).
     *
     * https://docs.microsoft.com/en-us/bot-framework/nodejs/bot-builder-nodejs-send-suggested-actions
     *
     * @param session
     * @param {string} messageText
     * @param {array} suggestedActions
     * @param {Message}
     */
    suggestedActionMessage: function(session, messageText, suggestedActions) {
        let msg = new builder.Message(session)
            .text(messageText)
            .suggestedActions(
                builder.SuggestedActions.create(
                    session, suggestedActions
                ));
    },


    /**
     * Card Attachment
     *
     * @param session
     * @param {array|object} cards
     *      Attach one or more cards to the message
     * @param {string} layoutType
     *      <carousel|list> Defaults to "list"
     * @returns {Message|*}
     */
    cardMessage: function(session, cards, layoutType) {
        let msg = new builder.Message(session);

        if (Array.isArray(cards)) {
            // Attaching an array of cards
            msg.attachments(cards);
            // Layout type must be "list" or "carousel"
            if (typeof layoutType === 'undefined' || ! (layoutType === 'list' || layoutType === 'carousel')) {
                layoutType = 'list';
            }
            msg.attachmentLayout(layoutType);
        } else {
            // Card is a single object
            msg.addAttachment(cards);
        }

        return msg;
    },

    /**
     * Carousel Card Attachment
     *
     * Modifies the layout of cardMessage to be displayed as a carousel.
     *
     * Carousel only supports these card types: Hero and Thumbnail.
     *
     * @param session
     * @param {array|object} cards
     *      Attach one or more cards to the message
     * @returns {Message|*}
     */
    carouselCardMessage: function(session, cards) {
        let layoutype = 'carousel';

        return module.exports.cardMessage(session, cards, layoutype)
    },

    /**
     *
     * @param session
     * @param {string} messageText
     * @param {string} textFormat
     *      <plain|markdown|xml> Defaults to "plain"
     *      https://docs.microsoft.com/en-us/bot-framework/bot-service-channel-inspector
     * @param {string} locale
     *      Defaults to "en-us"
     * @returns {Message}
     */
    textMessage: function(session, messageText, textFormat, locale) {
        let message = new builder.Message(session).text(messageText);

        // Validate text format type
        const allowedFormats = ['plain', 'markdown', 'xml'];
        if (typeof textFormat === 'undefined' || ! textFormat.indexOf(allowedFormats)) {
            textFormat = 'plain';
        }
        message.textFormat(textFormat);

        // Ensure language locale is set
        if (typeof locale === 'undefined') {
            locale = 'en-us';
        }
        message.textLocale(locale);

        return message;
    },

    /**
     * Animated Card for cardMessage
     *
     * A card that can play animated GIFs or short videos.
     *
     * https://docs.botframework.com/en-us/node/builder/chat-reference/classes/_botbuilder_d_.animationcard.html
     *
     * @returns {AnimationCard}
     */
    animationCard: function(session, animationUrl, options) {
        return mediaCardBuilder('animation', animationUrl, session, options, function (card) {

            return card;
        });
    },

    audioCard: function(session, audioFileUrl, options) {
        return mediaCardBuilder('audio', audioFileUrl, session, options, function (card) {

            return card;
        });
    },

    heroCard: function(session, images, options, buttons) {
        thumbnailCardBuilder('hero', session, images, options, buttons, function(card) {

            return card;
        });
    },

    imageCard: function(session, imageUrl, options) {

        let img = builder.CardImage.create(session, imageUrl);
        if (options) {
            if ("tapUrl" in options && ! buttons) {
                img.tap(module.exports.urlButton(session, options.tapUrl))
            }
            if ("tapImg" in options && ! buttons) {
                img.tap(builder.CardAction.showImage(session, options.tapImg))
            }
        }
    },

    /**
     * Media Card
     *
     * https://docs.botframework.com/en-us/node/builder/chat-reference/classes/_botbuilder_d_.mediacard.html
     *
     * @returns {AnimationCard}
     */
    mediaCard: function(session, mediaFileUrl, options) {
        return mediaCardBuilder('media', mediaFileUrl, session, options, function (card) {

            return card;
        });
    },

    receiptCard: function(session) {
        return new builder.ReceiptCard(session)
            .title('John Doe')
            .facts([
                builder.Fact.create(session, order ++, 'Order Number'),
                builder.Fact.create(session, 'VISA 5555-****', 'Payment Method')
            ])
            .items([
                builder.ReceiptItem.create(session, '$ 38.45', 'Data Transfer')
                       .quantity(368)
                       .image(builder.CardImage.create(session, 'https://github.com/amido/azure-vector-icons/raw/master/renders/traffic-manager.png')),
                builder.ReceiptItem.create(session, '$ 45.00', 'App Service')
                       .quantity(720)
                       .image(builder.CardImage.create(session, 'https://github.com/amido/azure-vector-icons/raw/master/renders/cloud-service.png'))
            ])
            .tax('$ 7.50')
            .total('$ 90.95')
            .buttons([
                builder.CardAction.openUrl(session, 'https://azure.microsoft.com/en-us/pricing/', 'More Information')
                       .image('https://raw.githubusercontent.com/amido/azure-vector-icons/master/renders/microsoft-azure.png')
            ]);
    },

    signinCard: function(session, messageText, buttonText) {
        return new builder.SigninCard(session)
            .text(messageText)
            .button('Sign-in', buttonText);
    },

    thumbnailCard: function(session, images, options, buttons) {
        thumbnailCardBuilder('thumbnail', session, images, options, buttons, function(card) {

            return card;
        });
    },

    /**
     *
     * @param session
     * @param videoUrl
     * @param options
     * @returns {VideoCard}
     */
    videoCard: function(session, videoUrl, options) {
        return mediaCardBuilder('media', mediaFileUrl, session, options, function (card) {

            return card;
        });
    },

    /**
     * PostBack Button/Action
     *
     * This Suggested Action method will post the clicked value back to the bot.
     * Some channels do not support postBack. For these channels, this method will behave like imBack.
     *
     * @param session
     * @param {string} title
     * @param {string} value
     * @returns {CardAction}
     */
    postbackButton: function(session, title, value) {

        return builder.CardAction.postBack(session, value, title);
    },

    /**
     * ImBack Button/Action
     *
     * This Suggested Action method will post the clicked value to the chat window of the channel you are using.
     *
     * @param session
     * @param {string} title
     * @param {string} value
     * @returns {CardAction}
     */
    imbackButton: function(session, title, value) {

        return builder.CardAction.imBack(session, value, title);
    },

    urlButton: function(session, url, title) {

        return builder.CardAction.openUrl(session, url, title)
    }

}


function thumbnailCardBuilder(type, session, imageUrls, options, buttons, callback) {
    new Promise(function (resolve, reject) {
        let card;
        if (type === 'hero') {
            resolve(builder.HeroCard(session));
        } else {
            resolve(builder.ThumbnailCard(session));
        }
    }).then(function(card) {
        console.log(card);
        return new Promise(function (resolve, reject) {
            // Set the option values
            if (options) {
                if ("title" in options) {
                    card.title(options.title);
                }
                if ("subtitle" in options) {
                    card.subtitle(options.subtitle);
                }
                if ("text" in options) {
                    card.text(options.text);
                }
                if ("tapUrl" in options && ! buttons) {
                    card.tap(module.exports.urlButton(session, options.tapUrl))
                }
            }

            // Add the image
            if (imageUrls) {
                let cardImages = [];
                if (! Array.isArray(imageUrls)) {
                    let imgUrl = imageUrls;
                    imageUrls = [];
                    imageUrls.push(imgUrl);
                }
                for (let i = 0; i < imageUrls.length; i++) {
                    let cardImg = builder.CardImage.create(session, imageUrls[i]);
                    cardImages.push(cardImg);
                }
                card.images(cardImages);
            }

            // Add any buttons
            if (buttons) {
                if (! Array.isArray(buttons)) {
                    let button = buttons;
                    buttons = [];
                    buttons.push(button);
                }
                card.buttons(buttons);
            }

            callback(card);
        });
    }).catch(function (error) {
        console.error(error.message);
        callback(null);
    });
}

/**
 * Media Card Builder (Animation, Audio, Video)
 *
 * https://docs.botframework.com/en-us/node/builder/chat-reference/classes/_botbuilder_d_.mediacard.html
 *
 * @param {string} mediaUrl
 * @param {string} mediaType
 * @param {object} options
 *      title: {string} Title of the Card
 *      subtitle: {string} Subtitle appears just below Title field, differs from Title in font styling only
 *      text: {string} Text field appears just below subtitle, differs from Subtitle in font styling only
 *      thumbnail: {string} Some channels (e.g. Skype) may require a thumbnail image
 *      autostart: {boolean} Should the media start automatically
 *      autoloop: {boolean} Should the media source reproduction run in a loop
 *      shareble: {boolean} Should media be shareable
 *  @param {array|object} buttons
 * @returns {AnimationCard}
 */
function mediaCardBuilder(mediaUrl, session, mediaType, options, buttons, callback) {
    new Promise(function (resolve, reject) {
        let card;
        switch(mediaType) {
            case 'animated':
                card = new builder.AnimationCard(session);
                break;
            case 'audio':
                card = new builder.AudioCard(session);
                break;
            case 'video':
                card = new builder.VideoCard(session);
                break;
            default:
                card = new builder.MediaCard(session);
        }
        resolve(card);
    }).then(function(card) {
        return new Promise(function (resolve, reject) {
            // Attach the media file
            card.media([
                {url: mediaUrl}
            ]);

            // Set the option values
            if (options) {
                if ("title" in options) {
                    card.title(options.title);
                }
                if ("subtitle" in options) {
                    card.subtitle(options.subtitle);
                }
                if ("thumbnail" in options) {
                    card.image(builder.CardImage.create(session, options.thumbnail))
                }
                if ("shareable" in options) {
                    card.shareable(options.shareable);
                }
                if ("autostart" in options) {
                    card.autostart(options.autostart);
                }
                if ("autoloop" in options) {
                    card.autoloop(options.autoloop);
                }
            }

            // Add any buttons
            if (buttons) {
                if (! Array.isArray(buttons)) {
                    let button = buttons;
                    buttons = [];
                    buttons.push(button);
                }
                card.buttons(buttons);
            }

            callback(card);
        });
    }).catch(function (error) {
        console.error(error.message);
        callback(null);
    });
}
