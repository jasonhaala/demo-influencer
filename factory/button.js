'use strict';

const builder = require('botbuilder');

module.exports = {

    build: function(session, btnType, btnData, callback) {
        switch(btnType.toLowerCase()) {
            case 'dialogaction':
                module.exports.dialogAction(session, btnData, callback);
                break;
            case 'url':
            case 'openurl':
                module.exports.openUrl(session, btnData, callback);
                break;
            case 'postback':
                module.exports.postBack(session, btnData, callback);
                break;
            case 'imback':
                module.exports.imBack(session, btnData, callback);
                break;
            case 'download':
            case 'downloadfile':
                module.exports.downloadFile(session, btnData, callback);
                break;
            case 'video':
            case 'playvideo':
                module.exports.playVideo(session, btnData, callback);
                break;
            case 'audio':
            case 'playaudio':
                module.exports.playAudio(session, btnData, callback);
                break;
            default:
                // Invalid button type. Unable to display.
                botlog.error('Invalid button type: ', btnType);

                callback('Invalid button type', null);
        }
    },

    downloadFile: function(session, data, callback) {

        return builder.CardAction.downloadFile(session, data.url, data.title);

        callback(null, button);
    },

    dialogAction: function(session, data, callback) {
        //console.log('data', data);
        if (typeof data.args === 'undefined') {
            data.args = null;
        }

        let button = builder.CardAction.dialogAction(session, data.value, data.arg, data.title);

        callback(null, button);
    },

    imBack: function(session, data, callback) {
        let button = builder.CardAction.openUrl(session, data.value, data.title);

        callback(null, button);
    },

    openUrl: function(session, data, callback) {
        let button = builder.CardAction.openUrl(session, data.value, data.title);

        callback(null, button);
    },

    playAudio: function(session, data, callback) {
        let button = builder.CardAction.playAudio(session, data.value, data.title);

        callback(null, button);
    },

    playVideo: function(session, data, callback) {
        let button = builder.CardAction.playVideo(session, data.value, data.title);

        callback(null, button);
    },

    postBack: function(session, data, callback) {
        let button = builder.CardAction.postBack(session, data.value, data.title);

        callback(null, button);
    },

}
